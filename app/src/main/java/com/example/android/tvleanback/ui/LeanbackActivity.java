package com.example.android.tvleanback.ui;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.android.tvleanback.R;
import com.example.android.tvleanback.Utils;
import com.example.android.tvleanback.model.Video;
import com.example.android.tvleanback.receiver.VideoReceiver;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * This parent class contains common methods that run in every activity such as search.
 */
public abstract class LeanbackActivity extends FragmentActivity {

    protected BroadcastReceiver br;

    @Override
    public boolean onSearchRequested() {
        startActivity(new Intent(this, SearchActivity.class));
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        VideoReceiver vr = new VideoReceiver();
        vr.isPlayer = this instanceof PlaybackActivity;
        br = vr;
        IntentFilter filter = new IntentFilter();
        filter.addAction(VideoReceiver.SYNC_ACTION);
        if(Utils.isTV(this)) {
            filter.addAction(VideoReceiver.VIDEO_ACTION);
            filter.addAction(VideoReceiver.PLAYER_ACTION);
        }
        registerReceiver(br, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(br);
    }

}
