/*
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.tvleanback.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.leanback.app.GuidedStepFragment;
import androidx.leanback.widget.GuidanceStylist;
import androidx.leanback.widget.GuidedAction;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.Toast;

import com.example.android.tvleanback.R;
import com.example.android.tvleanback.Utils;
import com.example.android.tvleanback.firebase.FirebaseManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class AuthenticationActivity extends Activity {
    private static final int CONTINUE = 2;
    private static final String TAG = "RegisterERR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialize Firebase Auth
        //mAuth = FirebaseAuth.getInstance();
        if (null == savedInstanceState) {
            GuidedStepFragment.addAsRoot(this, new FirstStepFragment(), android.R.id.content);
        }
    }

    public static class FirstStepFragment extends GuidedStepFragment {
        FirebaseAuth mAuth;
        @Override
        public int onProvideTheme() {
            return R.style.Theme_Example_Leanback_GuidedStep_First;
        }

        @Override
        @NonNull
        public GuidanceStylist.Guidance onCreateGuidance(@NonNull Bundle savedInstanceState) {
            mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            String title;
            String description;

            //If its a default anonymous user it will have his mail as ""
            assert currentUser != null;
            if(currentUser == null || currentUser.isAnonymous()){ // in other words, if no user is logged in:
                title = getString(R.string.pref_title_screen_signin);
                description = getString(R.string.pref_title_login_description);
            } else { // user is already logged in
                title = getString(R.string.pref_title_logout);
                description = getString(R.string.pref_title_logout_description);
            }
            Drawable icon = getActivity().getDrawable(R.drawable.ic_main_icon);
            return new GuidanceStylist.Guidance(title, description, "", icon);
        }

        @Override
        public void onCreateActions(@NonNull List<GuidedAction> actions, Bundle savedInstanceState) {

            mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if(currentUser == null || currentUser.isAnonymous()){
                GuidedAction enterUsername = new GuidedAction.Builder()
                        .title(getString(R.string.pref_title_username))
                        .descriptionEditable(true)
                        .build();
                GuidedAction enterPassword = new GuidedAction.Builder()
                        .title(getString(R.string.pref_title_password))
                        .descriptionEditable(true)
                        .descriptionInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        .descriptionEditInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        .build();
                enterPassword.getDescriptionEditInputType();
                GuidedAction login = new GuidedAction.Builder()
                        .id(CONTINUE)
                        .title(getString(R.string.guidedstep_continue))
                        .build();
                actions.add(enterUsername);
                actions.add(enterPassword);
                actions.add(login);
            }else{
                GuidedAction signout = new GuidedAction.Builder()
                        .id(CONTINUE)
                        .title(getString(R.string.pref_title_logout))
                        .build();
                actions.add(signout);
            }
        }

        @Override
        public void onGuidedActionClicked(GuidedAction action) {
            if (action.getId() == CONTINUE) {

                // Action for sign out
                if(action.getTitle() == getString(R.string.pref_title_logout)){
                    FirebaseAuth.getInstance().signOut();
                    Utils.showToast(getActivity(), getString(R.string.pref_toast_logout_success), Toast.LENGTH_SHORT);
                    // redirect to main login/register page
                    Utils.recreate = true;
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finishAfterTransition();
                } else { // Action for sign in
                    mAuth = FirebaseAuth.getInstance();
                    String username = getActions().get(0).getLabel2().toString();
                    String password = getActions().get(1).getLabel2().toString();

                    try {
                        mAuth.signInWithEmailAndPassword(username, password)
                                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "signInWithEmail:success");
                                            FirebaseUser user = mAuth.getCurrentUser();
                                            Utils.showToast(getActivity(), getString(R.string.pref_toast_login_success), Toast.LENGTH_SHORT);
                                            Utils.recreate = true;
                                            Intent i = new Intent(getActivity(), MainActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(i);
                                            getActivity().finishAfterTransition();
                                        } else {
                                            //Clear the password field
                                            for(GuidedAction g: getActions()){
                                                if(g.getTitle().equals(getString(R.string.pref_title_password))){
                                                    g.setDescription("");
                                                    getActivity().recreate();
                                                }
                                            }
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                                            Utils.showToast(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT);
                                        }
                                    }
                                });
                    } catch (Exception e){
                        Log.e("MSG", e.getMessage());
                        Utils.showToast(getActivity(), getString(R.string.pref_toast_fail), Toast.LENGTH_SHORT);
                    }
                }
                //getActivity().finishAfterTransition();
            }
        }
    }
}
