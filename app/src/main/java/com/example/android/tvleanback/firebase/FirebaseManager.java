package com.example.android.tvleanback.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.android.tvleanback.Utils;
import com.example.android.tvleanback.model.Video;
import com.example.android.tvleanback.receiver.VideoReceiver;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;
import java.util.UUID;

public class FirebaseManager {

    private static FirebaseManager instance;

    public static FirebaseManager getInstance() {
        if(instance == null) {
            instance = new FirebaseManager();
        }
        return instance;
    }

    public final static String DATA_DB = "videoData";
    public final static String ACTION_DB = "videoState";
    public final static String SYNC_DB = "sync";
    public final static String SYNC_DATA_DB = "syncData";

    private final static String LOGIN_TAG = "LOGIN";
    private final static String VIDEO_TAG = "VIDEO";
    private final static String STATE_TAG = "STATE";
    private final static String SYNC_TAG = "SYNC";

    private final DatabaseReference database;
    private final FirebaseAuth auth;

    private ValueEventListener prevDataListener;
    private ValueEventListener prevActionListener;

    private ValueEventListener prevSyncListener;
    private String syncUuid;

    private FirebaseManager() {
        database = FirebaseDatabase.getInstance().getReference();
        auth = FirebaseAuth.getInstance();
    }

    public void login(Activity activity) {
        if(auth.getCurrentUser() == null) {
            auth.signInAnonymously()
                    .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                //Sign in success
                                Log.d(LOGIN_TAG, "Anonymous login success");
                            } else {
                                //Sign in failure
                                Log.w(LOGIN_TAG, "Anonymous login failure", task.getException());
                            }
                        }
                    });
        } else {
            Log.d(LOGIN_TAG, "Already logged in user");
        }
    }

    public void videoRequest(Video video, Context context) {
        if(isSynced() && !Utils.isTV(context)) {
            database.child(SYNC_DATA_DB).child(syncUuid).child(DATA_DB).setValue(video);
        }
    }

    public void actionRequest(int action, Context context) {
        if(isSynced() && !Utils.isTV(context)) {
            database.child(SYNC_DATA_DB).child(syncUuid).child(ACTION_DB).setValue(action);
        }
    }

    private void addVideoDataListener(DatabaseReference ref, Context context) {

        //Clear value
        ref = ref.child(DATA_DB);
        ref.setValue(0);

        //Register listener
        prevDataListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {

                    //Get video and clear data
                    Video video = dataSnapshot.getValue(Video.class);
                    dataSnapshot.getRef().setValue(0);

                    //Send intent
                    Intent intent = new Intent();
                    intent.setAction(VideoReceiver.VIDEO_ACTION);
                    intent.putExtra(VideoReceiver.VIDEO_PARAM, video);
                    context.sendBroadcast(intent);

                } catch(Exception e) {
                    Log.d(VIDEO_TAG, "Video request attended");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Error getting updated data
                Log.w(VIDEO_TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        ref.addValueEventListener(prevDataListener);

    }

    private void addVideoStateListener(DatabaseReference ref, Context context) {

        //Clear value
        ref = ref.child(ACTION_DB);
        ref.setValue(-1);

        //Register listener
        prevActionListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Get video state and clear data
                int action = dataSnapshot.getValue(Integer.class);
                if(action == -1) {
                    Log.d(STATE_TAG, "Video action request attended");
                } else {

                    //Clear action request
                    dataSnapshot.getRef().setValue(-1);

                    //Send intent
                    Intent intent = new Intent();
                    intent.setAction(VideoReceiver.PLAYER_ACTION);
                    intent.putExtra(VideoReceiver.PLAYER_PARAM, action);
                    context.sendBroadcast(intent);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Error getting updated data
                Log.w(STATE_TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        ref.addValueEventListener(prevActionListener);

    }

    public void syncRequest(Utils.StringCallback uuidCallback, Utils.BooleanCallback syncCallback,
                            Context context) {
        String uuid = UUID.randomUUID().toString().toUpperCase(Locale.ROOT).substring(0, 8);
        DatabaseReference ref = database.child(SYNC_DB).child(uuid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    //UUID exists in the database
                    syncRequest(uuidCallback, syncCallback, context);
                } else {
                    //UUID doesn't exist
                    ref.setValue(0);
                    addSyncListener(uuid, syncCallback, context);
                    uuidCallback.callFunctionWithParam(uuid);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                uuidCallback.callFunctionWithParam(null);
            }
        });
    }

    public void syncCheck(String uuid, Utils.BooleanCallback syncCallback) {
        DatabaseReference ref = database.child(SYNC_DB).child(uuid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                syncCallback.callFunctionWithParam(dataSnapshot.exists() && dataSnapshot.getValue(Integer.class) == 0);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                syncCallback.callFunctionWithParam(false);
            }
        });
    }

    public void syncLink(String uuid, Utils.BooleanCallback syncCallback, Context context) {
        database.child(SYNC_DB).child(uuid).setValue(1);
        addSyncListener(uuid, syncCallback, context);
    }

    private void addSyncListener(String uuid, Utils.BooleanCallback syncCallback, Context context) {

        //Get base reference
        DatabaseReference ref = database.child(SYNC_DB).child(uuid);
        syncUuid = uuid;

        //Register listener
        prevSyncListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    int syncStatus = dataSnapshot.getValue(Integer.class);
                    if(syncStatus == 1) {
                        addVideoDataListener(database.child(SYNC_DATA_DB).child(syncUuid), context);
                        addVideoStateListener(database.child(SYNC_DATA_DB).child(syncUuid), context);
                        syncCallback.callFunctionWithParam(true);
                    }
                } catch(NullPointerException e) {

                    //Disable sync
                    disableSync();

                    //Send intent
                    Intent intent = new Intent();
                    intent.setAction(VideoReceiver.SYNC_ACTION);
                    intent.putExtra(VideoReceiver.SYNC_PARAM, false);
                    context.sendBroadcast(intent);

                    //Notify activity
                    syncCallback.callFunctionWithParam(false);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Error getting updated data
                Log.w(SYNC_TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        ref.addValueEventListener(prevSyncListener);

    }

    public void disableSync() {
        if(isSynced()) {

            //Remove sync param
            DatabaseReference ref = database.child(SYNC_DB).child(syncUuid);
            ref.removeEventListener(prevSyncListener);
            ref.removeValue();

            //Remove video data param
            if(prevDataListener != null) {
                ref = database.child(SYNC_DATA_DB).child(syncUuid).child(DATA_DB);
                ref.removeEventListener(prevDataListener);
                ref.removeValue();
            }

            //Remove video action param
            if(prevActionListener != null) {
                ref = database.child(SYNC_DATA_DB).child(syncUuid).child(ACTION_DB);
                ref.removeEventListener(prevActionListener);
                ref.removeValue();
            }

        }

        //Clear listeners and sync UUID
        syncUuid = null;
        prevSyncListener = null;
        prevDataListener = null;
        prevActionListener = null;

    }

    public boolean isSynced() {
        return syncUuid != null;
    }

}
