/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.tvleanback.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.leanback.media.PlaybackTransportControlGlue;
import androidx.leanback.media.PlayerAdapter;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.PlaybackControlsRow;

import com.example.android.tvleanback.R;
import com.example.android.tvleanback.Utils;
import com.example.android.tvleanback.firebase.FirebaseManager;
import com.example.android.tvleanback.model.Video;
import com.example.android.tvleanback.ui.PlaybackActivity;
import com.example.android.tvleanback.ui.VideoDetailsActivity;
import com.google.android.exoplayer2.ext.leanback.LeanbackPlayerAdapter;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Manages customizing the actions in the {@link PlaybackControlsRow}. Adds and manages the
 * following actions to the primary and secondary controls:
 *
 * <ul>
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.RepeatAction}
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.ThumbsDownAction}
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.ThumbsUpAction}
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.SkipPreviousAction}
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.SkipNextAction}
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.FastForwardAction}
 *   <li>{@link androidx.leanback.widget.PlaybackControlsRow.RewindAction}
 * </ul>
 *
 * Note that the superclass, {@link PlaybackTransportControlGlue}, manages the playback controls
 * row.
 */
public class VideoPlayerGlue extends PlaybackTransportControlGlue<LeanbackPlayerAdapter> {

    private static final int PLAY_ACTION = 0;
    private static final int PREVIOUS_ACTION = 1;
    private static final int REWIND_ACTION = 2;
    private static final int FORWARD_ACTION = 3;
    private static final int NEXT_ACTION = 4;
    private static final int PAUSE_ACTION = 5;

    private static final int ENABLE_CAST_ACTION = 10;
    private static final int DISABLE_CAST_ACTION = 11;

    private static final long TEN_SECONDS = TimeUnit.SECONDS.toMillis(10);
    private static final int[] ACTIONS = {
            PLAY_ACTION,
            PREVIOUS_ACTION,
            REWIND_ACTION,
            FORWARD_ACTION,
            NEXT_ACTION
    };

    private final int playIndex;

    private final FirebaseManager fbManager;

    /** Listens for when skip to next and previous actions have been dispatched. */
    public interface OnActionClickedListener {

        /** Skip to the previous item in the queue. */
        void onPrevious();

        /** Skip to the next item in the queue. */
        void onNext();
    }

    private final OnActionClickedListener mActionListener;

    private PlaybackControlsRow.SkipPreviousAction mSkipPreviousAction;
    private PlaybackControlsRow.SkipNextAction mSkipNextAction;
    private PlaybackControlsRow.FastForwardAction mFastForwardAction;
    private PlaybackControlsRow.RewindAction mRewindAction;
    private PlaybackControlsRow.MultiAction mCastAction;
    private ArrayObjectAdapter mAdapter;
    private ArrayObjectAdapter mCastAdapter;

    private Context mContext;

    public VideoPlayerGlue(
            Context context,
            LeanbackPlayerAdapter playerAdapter,
            OnActionClickedListener actionListener) {
        super(context, playerAdapter);

        mActionListener = actionListener;

        //Create actions
        mSkipPreviousAction = new PlaybackControlsRow.SkipPreviousAction(context);
        mSkipNextAction = new PlaybackControlsRow.SkipNextAction(context);
        mFastForwardAction = new PlaybackControlsRow.FastForwardAction(context);
        mRewindAction = new PlaybackControlsRow.RewindAction(context);
        mCastAction = new CastAction(context);

        //Get play/pause action index
        playIndex = Arrays.binarySearch(ACTIONS, PLAY_ACTION);

        //Keep context
        mContext = context;

        //Get firebase manager
        fbManager = FirebaseManager.getInstance();

    }

    @Override
    protected void onCreatePrimaryActions(ArrayObjectAdapter adapter) {
        // Order matters, super.onCreatePrimaryActions() will create the play / pause action.
        // Will display as follows:
        // play/pause, previous, rewind, fast forward, next
        //   > /||      |<        <<        >>         >|
        super.onCreatePrimaryActions(adapter);
        adapter.add(mSkipPreviousAction);
        adapter.add(mRewindAction);
        adapter.add(mFastForwardAction);
        adapter.add(mSkipNextAction);
        mAdapter = adapter;
    }

    @Override
    protected void onCreateSecondaryActions(ArrayObjectAdapter adapter) {
        super.onCreateSecondaryActions(adapter);
        adapter.add(mCastAction);
        mCastAdapter = adapter;
        if(fbManager.isSynced()) {
            dispatchAction(mCastAction);
        }
    }

    @Override
    public void onActionClicked(Action action) {
        notifyActionToFirebase(getAction(String.valueOf(action.getLabel1())));
        if(shouldDispatchAction(action)) {
            dispatchAction(action);
            return;
        }
        // Super class handles play/pause and delegates to abstract methods next()/previous().
        super.onActionClicked(action);
    }

    // Should dispatch actions that the super class does not supply callbacks for.
    private boolean shouldDispatchAction(Action action) {
        return action == mRewindAction
                || action == mFastForwardAction
                || action == mCastAction;
    }

    private boolean isPlaylistAction(Action action) {
        return action == mSkipPreviousAction
                || action == mSkipNextAction;
    }

    private void dispatchAction(Action action) {
        // Primary actions are handled manually.
        if (action == mRewindAction) {
            rewind();
        } else if (action == mFastForwardAction) {
            fastForward();
        } else if(action instanceof PlaybackControlsRow.MultiAction) {  //Cast action included
            PlaybackControlsRow.MultiAction multiAction = (PlaybackControlsRow.MultiAction) action;
            multiAction.nextIndex();
            // Notify adapter of action changes to handle secondary actions, such as, thumbs up/down
            // and repeat.
            notifyActionChanged(
                    multiAction,
                    (ArrayObjectAdapter) getControlsRow().getSecondaryActionsAdapter());
        }
    }

    private void notifyActionChanged(
            PlaybackControlsRow.MultiAction action, ArrayObjectAdapter adapter) {
        if (adapter != null) {
            int index = adapter.indexOf(action);
            if (index >= 0) {
                adapter.notifyArrayItemRangeChanged(index, 1);
            }
        }
    }

    @Override
    public void next() {
        mActionListener.onNext();
    }

    @Override
    public void previous() {
        mActionListener.onPrevious();
    }

    /** Skips backwards 10 seconds. */
    public void rewind() {
        long newPosition = getCurrentPosition() - TEN_SECONDS;
        newPosition = (newPosition < 0) ? 0 : newPosition;
        getPlayerAdapter().seekTo(newPosition);
    }

    /** Skips forward 10 seconds. */
    public void fastForward() {
        if (getDuration() > -1) {
            long newPosition = getCurrentPosition() + TEN_SECONDS;
            newPosition = (newPosition > getDuration()) ? getDuration() : newPosition;
            getPlayerAdapter().seekTo(newPosition);
        }
    }

    private int getAction(String label) {
        for(int i = 0; i < mAdapter.size(); i++) {
            Action action = (Action) mAdapter.get(i);
            if(action.getLabel1().toString().compareToIgnoreCase(label) == 0) {
                if(i == playIndex) {
                    PlaybackControlsRow.PlayPauseAction pAction = (PlaybackControlsRow.PlayPauseAction) action;
                    if(pAction.getIndex() == 0) {
                        return ACTIONS[i];
                    } else {
                        return PAUSE_ACTION;
                    }
                } else {
                    return ACTIONS[i];
                }
            } else {
                if(i == playIndex) {
                    PlaybackControlsRow.PlayPauseAction pAction = (PlaybackControlsRow.PlayPauseAction) action;
                    String hiddenLabel = pAction.getLabel(Math.abs(pAction.getIndex() - 1));
                    if(hiddenLabel.compareToIgnoreCase(label) == 0) {
                        return PAUSE_ACTION;
                    }
                }
            }
        }
        if(mCastAction.getLabel1().toString().compareToIgnoreCase(label) == 0) {
            if(mCastAction.getIndex() == 0) {
                return ENABLE_CAST_ACTION;
            } else {
                return DISABLE_CAST_ACTION;
            }
        }
        return -1;
    }

    private void notifyActionToFirebase(int action) {
        if(action == ENABLE_CAST_ACTION) {
            if(Utils.isTV(mContext)) {
                syncWithSmartphone();
            } else {
                syncWithTV();
            }
        } else if(action == DISABLE_CAST_ACTION) {
            fbManager.disableSync();
            Utils.showToast(mContext, mContext.getString(R.string.syncopt_desync), Toast.LENGTH_SHORT);
        } else {
            //Notify action
            fbManager.actionRequest(action, mContext);
        }
    }

    private void syncWithTV() {
        boolean wasPlaying = isPlaying();
        if(wasPlaying) {
            super.onActionClicked((Action) mAdapter.get(playIndex));
        }
        fbManager.syncRequest(new Utils.StringCallback() {
            @Override
            public boolean callFunctionWithParam(String param) {
                if(param == null) {
                    syncCancel(wasPlaying);
                    Utils.showToast(mContext, mContext.getString(R.string.syncopt_error), Toast.LENGTH_SHORT);
                    return false;
                } else {
                    //Notify user
                    Utils.showProgressDialog(mContext, mContext.getString(R.string.syncopt_id_smartphone) + "\n" + param,
                            new Utils.GenericCallback() {
                                @Override
                                public boolean callFunction() {
                                    fbManager.disableSync();
                                    syncCancel(wasPlaying);
                                    return true;
                                }
                            });
                }
                return true;
            }
        }, new Utils.BooleanCallback() {
            @Override
            public boolean callFunctionWithParam(boolean param) {
                return successfulSync(param);
            }
        }, mContext);
    }

    private void syncWithSmartphone() {
        boolean wasPlaying = isPlaying();
        if(wasPlaying) {
            super.onActionClicked((Action) mAdapter.get(playIndex));
        }
        Utils.showTextInputDialog(mContext, mContext.getString(R.string.syncopt_id_tv), new Utils.GenericCallback() {
            @Override
            public boolean callFunction() {
                syncCancel(wasPlaying);
                return true;
            }
        }, mContext.getString(R.string.syncopt_start), new Utils.StringCallback() {
            @Override
            public boolean callFunctionWithParam(String param) {
                String uuid = param;
                if(uuid != null && uuid.length() == 8) {
                    //Check existent UUID
                    fbManager.syncCheck(uuid, new Utils.BooleanCallback() {
                        @Override
                        public boolean callFunctionWithParam(boolean param) {
                            Utils.closeDialog();
                            if(param) {
                                Utils.showProgressDialog(mContext, mContext.getString(R.string.syncopt_syncing), new Utils.GenericCallback() {
                                    @Override
                                    public boolean callFunction() {
                                        fbManager.disableSync();
                                        syncCancel(wasPlaying);
                                        Utils.showToast(mContext, mContext.getString(R.string.syncopt_interrupt), Toast.LENGTH_SHORT);
                                        return true;
                                    }
                                });
                                fbManager.syncLink(uuid, new Utils.BooleanCallback() {
                                    @Override
                                    public boolean callFunctionWithParam(boolean param) {
                                        return successfulSync(param);
                                    }
                                }, mContext);
                            } else {
                                Utils.showToast(mContext, mContext.getString(R.string.syncopt_null_id), Toast.LENGTH_SHORT);
                                syncCancel(wasPlaying);
                            }
                            return param;
                        }
                    });
                } else {
                    Utils.showToast(mContext, mContext.getString(R.string.syncopt_invalid_id), Toast.LENGTH_SHORT);
                }
                return false;
            }
        });
    }

    private void syncCancel(boolean wasPlaying) {
        updateSyncButton();
        if(wasPlaying) {
            VideoPlayerGlue.super.onActionClicked((Action) mAdapter.get(playIndex));
        }
    }

    private void updateSyncButton() {
        mCastAction.nextIndex();
        notifyActionChanged(mCastAction, mCastAdapter);
    }

    private boolean successfulSync(boolean isSynced) {
        Utils.closeDialog();
        Activity activity = (Activity) mContext;
        if(isSynced) { //Sync successful
            Log.d("SYNC", "Succesful sync!");
            Utils.showToast(mContext, mContext.getString(R.string.syncopt_success), Toast.LENGTH_SHORT);
            if(!Utils.isTV(mContext)) {
                Video video = activity.getIntent().getParcelableExtra(VideoDetailsActivity.VIDEO);
                Intent intent = new Intent(activity, PlaybackActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);
                fbManager.videoRequest(video, mContext);
                activity.finish();
                activity.startActivity(intent);
            } else {
                Utils.closeDialog();
            }
        } else {
            Log.d("SYNC", "Unsuccesful sync!");
            Utils.showToast(mContext, mContext.getString(R.string.syncopt_error), Toast.LENGTH_SHORT);
        }
        return true;
    }

    public void onActionRequest(int action) {
        switch(action) {
            case PLAY_ACTION:
            case PAUSE_ACTION:
                onActionClicked((Action) mAdapter.get(playIndex));
                break;
            case PREVIOUS_ACTION:
                onActionClicked(mSkipPreviousAction);
                break;
            case NEXT_ACTION:
                onActionClicked(mSkipNextAction);
                break;
            case REWIND_ACTION:
                onActionClicked(mRewindAction);
                break;
            case FORWARD_ACTION:
                onActionClicked(mFastForwardAction);
                break;
        }
    }

    public void notifySync(boolean syncMode) {
        if((syncMode && mCastAction.getIndex() == 0) || !syncMode && mCastAction.getIndex() != 0) {
            if(syncMode && mCastAction.getIndex() == 0) {
                Utils.showToast(mContext, mContext.getString(R.string.syncopt_success), Toast.LENGTH_SHORT);
            } else {
                Utils.showToast(mContext, mContext.getString(R.string.syncopt_desync), Toast.LENGTH_SHORT);
            }
            updateSyncButton();
        }
    }

}
