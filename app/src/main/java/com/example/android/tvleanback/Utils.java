/*
 * Copyright (c) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.tvleanback;

import static android.content.Context.UI_MODE_SERVICE;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.UiModeManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.HashMap;
import java.util.Locale;

/**
 * A collection of utility methods, all static.
 */
public class Utils {

    private static final String DEVICE_TYPE_TAG = "DeviceTypeRuntimeCheck";

    public static boolean onBoarding = true;
    public static boolean recreate = false;

    private static Toast toast;
    private static Dialog dialog;

    public interface MediaDimensions {
        double MEDIA_HEIGHT = 0.95;
        double MEDIA_WIDTH = 0.95;
        double MEDIA_TOP_MARGIN = 0.025;
        double MEDIA_RIGHT_MARGIN = 0.025;
        double MEDIA_BOTTOM_MARGIN = 0.025;
        double MEDIA_LEFT_MARGIN = 0.025;
    }

    /*
     * Making sure public utility methods remain static
     */
    private Utils() {
    }

    /**
     * Returns the screen/display size.
     */
    public static Point getDisplaySize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // You can get the height & width like such:
        // int width = size.x;
        // int height = size.y;
        return size;
    }

    public static int convertDpToPixel(Context ctx, int dp) {
        float density = ctx.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    /**
     * Example for handling resizing content for overscan.  Typically you won't need to resize when
     * using the Leanback support library.
     */
    public void overScan(Activity activity, VideoView videoView) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int w = (int) (metrics.widthPixels * MediaDimensions.MEDIA_WIDTH);
        int h = (int) (metrics.heightPixels * MediaDimensions.MEDIA_HEIGHT);
        int marginLeft = (int) (metrics.widthPixels * MediaDimensions.MEDIA_LEFT_MARGIN);
        int marginTop = (int) (metrics.heightPixels * MediaDimensions.MEDIA_TOP_MARGIN);
        int marginRight = (int) (metrics.widthPixels * MediaDimensions.MEDIA_RIGHT_MARGIN);
        int marginBottom = (int) (metrics.heightPixels * MediaDimensions.MEDIA_BOTTOM_MARGIN);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(w, h);
        lp.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        videoView.setLayoutParams(lp);
    }

    public static long getDuration(String videoUrl) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            mmr.setDataSource(videoUrl, new HashMap<>());
        } else {
            mmr.setDataSource(videoUrl);
        }
        return Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
    }

    /**
     * Displays a Toast, preventing multiple Toast spam
     */
    public static void showToast(Context context, String text, int duration) {
        if(toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    /**
     * Displays a Progress dialog, preventing multiple Dialog spam
     */
    public static void showProgressDialog(Context context, String message,
                                          GenericCallback onCancel) {

        if(dialog != null) {
            dialog.dismiss();
        }

        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setCancelable(true);
        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                onCancel.callFunction();
            }
        });

        dialog = pDialog;
        dialog.show();

    }

    /**
     * Displays a Text Input dialog, preventing multiple Dialog spam
     */
    public static void showTextInputDialog(Context context, String message,
                                           GenericCallback onCancel,
                                           String okButtonMessage,
                                           StringCallback onOk) {

        if(dialog != null) {
            dialog.dismiss();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(message);
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                onCancel.callFunction();
            }
        });

        //Set input text
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        //Config ok button
        builder.setPositiveButton(okButtonMessage, null);

        AlertDialog aDialog = builder.create();
        aDialog.show();
        Button syncButton = aDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onOk.callFunctionWithParam(input.getText().toString().toUpperCase(Locale.ROOT))) {
                    syncButton.setEnabled(false);
                }
            }
        });
        dialog = aDialog;

    }

    /**
     * Closes dialog if exists
     */
    public static void closeDialog() {
        if(dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    /**
     * Gets file extension
     */
    public static String getFileExtension(String fileName) {
        if(fileName.contains(".")) {
            String posExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
            if(posExtension.contains("?")) {
                return posExtension.substring(0, posExtension.indexOf("?"));
            }
            return posExtension;
        } else {
            return "";
        }
    }

    /**
     * Notifies if system is a TV
     */
    public static boolean isTV(Context context) {
        UiModeManager uiModeManager = (UiModeManager) context.getSystemService(UI_MODE_SERVICE);
        if(uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION) {
            Log.d(DEVICE_TYPE_TAG, "Running on a TV Device");
            return true;
        } else {
            Log.d(DEVICE_TYPE_TAG, "Running on a non-TV Device");
            return false;
        }
    }

    public interface GenericCallback {
        boolean callFunction();
    }

    public interface StringCallback {
        boolean callFunctionWithParam(String param);
    }

    public interface BooleanCallback {
        boolean callFunctionWithParam(boolean param);
    }

}
