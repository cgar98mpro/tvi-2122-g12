package com.example.android.tvleanback.player;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.leanback.widget.PlaybackControlsRow;

import com.example.android.tvleanback.R;

public class CastAction extends PlaybackControlsRow.MultiAction {

    private final static int CAST_ID = -1;

    public CastAction(Context context) {
        super(CAST_ID);
        Drawable[] drawables = new Drawable[]{context.getDrawable(R.drawable.cast_off), context.getDrawable(R.drawable.cast_on)};
        this.setDrawables(drawables);
        String[] labels = new String[]{context.getString(R.string.player_cast_enable), context.getString(R.string.player_cast_disable)};
        this.setLabels(labels);
    }

}
