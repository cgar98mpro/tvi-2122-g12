package com.example.android.tvleanback.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.android.tvleanback.R;
import com.example.android.tvleanback.Utils;
import com.example.android.tvleanback.model.Video;
import com.example.android.tvleanback.ui.PlaybackActivity;
import com.example.android.tvleanback.ui.VideoDetailsActivity;

public class VideoReceiver extends BroadcastReceiver {

    public static final String VIDEO_ACTION = "NEW_VIDEO";
    public static final String PLAYER_ACTION = "NEW_ACTION";
    public static final String SYNC_ACTION = "SYNC_ACTION";

    public static final String VIDEO_PARAM = VideoDetailsActivity.VIDEO;
    public static final String PLAYER_PARAM = "State";
    public static final String SYNC_PARAM = "SyncMode";

    private static final String TAG = "VIDEO_RECEIVER";
    public boolean isPlayer = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        //Check message
        Log.d(TAG, "RECEIVED MSG");
        if(intent.getAction().compareToIgnoreCase(PLAYER_ACTION) == 0) {
            Log.d("VR_PLAYER", "New action from player");
            int action = intent.getIntExtra(PLAYER_PARAM, -1);
            if(isPlayer) {
                ((PlaybackActivity) context).requestAction(action);
            }
        } else if(intent.getAction().compareToIgnoreCase(VIDEO_ACTION) == 0) {
            Log.d("VR_VIDEO", "New video to play");
            Video video = intent.getParcelableExtra(VIDEO_PARAM);
            Intent i = new Intent(context, PlaybackActivity.class);
            i.putExtra(VideoDetailsActivity.VIDEO, video);
            if(isPlayer) {
                ((Activity) context).finish();
            }
            context.startActivity(i);
        } else if(intent.getAction().compareToIgnoreCase(SYNC_ACTION) == 0) {
            boolean syncMode = intent.getBooleanExtra(SYNC_PARAM, false);
            if(syncMode) {
                Log.d("VR_SYNC", "Sync action");
            } else {
                Log.d("VR_SYNC", "Desync action");
            }
            if(isPlayer) {
                ((PlaybackActivity) context).notifySync(syncMode);
            } else {
                Utils.showToast(context, context.getString(R.string.syncopt_desync), Toast.LENGTH_SHORT);
            }
        }
    }

}
