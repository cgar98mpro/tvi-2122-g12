package com.example.android.tvleanback.data;

import android.content.ContentValues;

import java.util.List;

public interface DataReadyCallback {
    void onDataReady(List<ContentValues> contentValues);
}
